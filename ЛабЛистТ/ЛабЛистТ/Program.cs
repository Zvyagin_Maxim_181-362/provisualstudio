﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ЛабЛистТ
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> x = new List<int>() { 1, 2, 3, 45 };
            x.Add(6);

            x.AddRange(new int[] { 7, 8, 9 });
            x.Insert(0, 500);
            x.Remove(1);

            foreach(int i in x)
            {
                Console.WriteLine(i);
            }
        }
    }
}
