﻿namespace Я_сошел_с_ума_1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Startbtn = new System.Windows.Forms.Button();
            this.l1 = new System.Windows.Forms.Label();
            this.l2 = new System.Windows.Forms.Label();
            this.l3 = new System.Windows.Forms.Label();
            this.Dvig1 = new System.Windows.Forms.Timer(this.components);
            this.Dvig2 = new System.Windows.Forms.Timer(this.components);
            this.Dvig3 = new System.Windows.Forms.Timer(this.components);
            this.Stop1 = new System.Windows.Forms.Timer(this.components);
            this.Stop2 = new System.Windows.Forms.Timer(this.components);
            this.Stop3 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // Startbtn
            // 
            this.Startbtn.BackColor = System.Drawing.Color.White;
            this.Startbtn.Font = new System.Drawing.Font("Wide Latin", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Startbtn.Location = new System.Drawing.Point(58, 319);
            this.Startbtn.Name = "Startbtn";
            this.Startbtn.Size = new System.Drawing.Size(281, 92);
            this.Startbtn.TabIndex = 0;
            this.Startbtn.Text = "SPIN";
            this.Startbtn.UseVisualStyleBackColor = false;
            this.Startbtn.Click += new System.EventHandler(this.Startbtn_Click);
            // 
            // l1
            // 
            this.l1.AutoSize = true;
            this.l1.BackColor = System.Drawing.Color.White;
            this.l1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l1.Font = new System.Drawing.Font("Old English Text MT", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l1.Location = new System.Drawing.Point(8, 174);
            this.l1.Name = "l1";
            this.l1.Size = new System.Drawing.Size(100, 116);
            this.l1.TabIndex = 1;
            this.l1.Text = "0";
            // 
            // l2
            // 
            this.l2.AutoSize = true;
            this.l2.BackColor = System.Drawing.Color.White;
            this.l2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l2.Font = new System.Drawing.Font("Old English Text MT", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l2.Location = new System.Drawing.Point(142, 174);
            this.l2.Name = "l2";
            this.l2.Size = new System.Drawing.Size(100, 116);
            this.l2.TabIndex = 2;
            this.l2.Text = "0";
            // 
            // l3
            // 
            this.l3.AutoSize = true;
            this.l3.BackColor = System.Drawing.Color.White;
            this.l3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l3.Font = new System.Drawing.Font("Old English Text MT", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l3.Location = new System.Drawing.Point(275, 174);
            this.l3.Name = "l3";
            this.l3.Size = new System.Drawing.Size(100, 116);
            this.l3.TabIndex = 3;
            this.l3.Text = "0";
            // 
            // Dvig1
            // 
            this.Dvig1.Interval = 10;
            this.Dvig1.Tick += new System.EventHandler(this.Dvig1_Tick);
            // 
            // Dvig2
            // 
            this.Dvig2.Interval = 10;
            this.Dvig2.Tick += new System.EventHandler(this.Dvig2_Tick);
            // 
            // Dvig3
            // 
            this.Dvig3.Interval = 10;
            this.Dvig3.Tick += new System.EventHandler(this.Dvig3_Tick);
            // 
            // Stop1
            // 
            this.Stop1.Interval = 1500;
            this.Stop1.Tick += new System.EventHandler(this.Stop1_Tick);
            // 
            // Stop2
            // 
            this.Stop2.Interval = 2500;
            this.Stop2.Tick += new System.EventHandler(this.Stop2_Tick);
            // 
            // Stop3
            // 
            this.Stop3.Interval = 3000;
            this.Stop3.Tick += new System.EventHandler(this.Stop3_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gold;
            this.ClientSize = new System.Drawing.Size(387, 435);
            this.Controls.Add(this.l3);
            this.Controls.Add(this.l2);
            this.Controls.Add(this.l1);
            this.Controls.Add(this.Startbtn);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Startbtn;
        private System.Windows.Forms.Label l1;
        private System.Windows.Forms.Label l2;
        private System.Windows.Forms.Label l3;
        private System.Windows.Forms.Timer Dvig1;
        private System.Windows.Forms.Timer Dvig2;
        private System.Windows.Forms.Timer Dvig3;
        private System.Windows.Forms.Timer Stop1;
        private System.Windows.Forms.Timer Stop2;
        private System.Windows.Forms.Timer Stop3;
    }
}

