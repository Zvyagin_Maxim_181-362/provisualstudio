﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Я_сошел_с_ума_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Dvig1_Tick(object sender, EventArgs e)
        {
            Random rand = new Random();
            int dig = rand.Next(8);
            l1.Text = System.Convert.ToString(dig);
        }

        private void Dvig2_Tick(object sender, EventArgs e)
        {
            Random rand = new Random();
            int dig = rand.Next(8);
            l2.Text = System.Convert.ToString(dig);

        }

        private void Dvig3_Tick(object sender, EventArgs e)
        {
            Random rand = new Random();
            int dig = rand.Next(8);
            l3.Text = System.Convert.ToString(dig);

        }

        private void Startbtn_Click(object sender, EventArgs e)
        {
            Dvig1.Enabled = true;
            Dvig2.Enabled = true;
            Dvig3.Enabled = true;
            Stop1.Enabled = true;
            Stop2.Enabled = true;
            Stop3.Enabled = true;


        }

        private void Stop1_Tick(object sender, EventArgs e)
        {
            Dvig1.Enabled = false;
            Stop1.Enabled = false;

        }

        private void Stop2_Tick(object sender, EventArgs e)
        {
            Dvig2.Enabled = false;
            Stop2.Enabled = false;
        }

        private void Stop3_Tick(object sender, EventArgs e)
        {
            Dvig3.Enabled = false;
            Stop3.Enabled = false;

            if ((l1.Text == "1") && (l2.Text == "1"))
            {
                MessageBox.Show("Вы выиграли 40 хривен");

            }
            if ((l1.Text == "2") && (l2.Text == "2"))
            {
                MessageBox.Show("Вы выиграли 90 хривен");

            }
            if ((l1.Text == "3") && (l2.Text == "3"))
            {
                MessageBox.Show("Вы выиграли 3330 хривен");


            }
            if ((l1.Text == "4") && (l2.Text == "4"))
            {
                MessageBox.Show("Вы выиграли 440 хривен");

            }
            if ((l1.Text == "5") && (l2.Text == "5"))
            {
                MessageBox.Show("Вы выиграли 5590 хривен");

            }
            if ((l1.Text == "6") && (l2.Text == "6"))
            {
                MessageBox.Show("Вы выиграли 6660 хривен");
            }



        }
    }
}
    
