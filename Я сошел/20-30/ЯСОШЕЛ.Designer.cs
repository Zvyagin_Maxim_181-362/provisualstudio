﻿namespace Я_сошел_с_ума_1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Startbtn = new System.Windows.Forms.Button();
            this.O1 = new System.Windows.Forms.Label();
            this.O2 = new System.Windows.Forms.Label();
            this.O3 = new System.Windows.Forms.Label();
            this.D1 = new System.Windows.Forms.Timer(this.components);
            this.D2 = new System.Windows.Forms.Timer(this.components);
            this.D3 = new System.Windows.Forms.Timer(this.components);
            this.S1 = new System.Windows.Forms.Timer(this.components);
            this.S2 = new System.Windows.Forms.Timer(this.components);
            this.S3 = new System.Windows.Forms.Timer(this.components);
            this.Label2 = new System.Windows.Forms.Label();
            this.O21 = new System.Windows.Forms.Label();
            this.O22 = new System.Windows.Forms.Label();
            this.O53 = new System.Windows.Forms.Label();
            this.O52 = new System.Windows.Forms.Label();
            this.O51 = new System.Windows.Forms.Label();
            this.O32 = new System.Windows.Forms.Label();
            this.O43 = new System.Windows.Forms.Label();
            this.O42 = new System.Windows.Forms.Label();
            this.O41 = new System.Windows.Forms.Label();
            this.O31 = new System.Windows.Forms.Label();
            this.O33 = new System.Windows.Forms.Label();
            this.O23 = new System.Windows.Forms.Label();
            this.D21 = new System.Windows.Forms.Timer(this.components);
            this.D22 = new System.Windows.Forms.Timer(this.components);
            this.D23 = new System.Windows.Forms.Timer(this.components);
            this.D31 = new System.Windows.Forms.Timer(this.components);
            this.D32 = new System.Windows.Forms.Timer(this.components);
            this.D33 = new System.Windows.Forms.Timer(this.components);
            this.D41 = new System.Windows.Forms.Timer(this.components);
            this.D42 = new System.Windows.Forms.Timer(this.components);
            this.D43 = new System.Windows.Forms.Timer(this.components);
            this.D51 = new System.Windows.Forms.Timer(this.components);
            this.D52 = new System.Windows.Forms.Timer(this.components);
            this.D53 = new System.Windows.Forms.Timer(this.components);
            this.S21 = new System.Windows.Forms.Timer(this.components);
            this.S22 = new System.Windows.Forms.Timer(this.components);
            this.S23 = new System.Windows.Forms.Timer(this.components);
            this.S31 = new System.Windows.Forms.Timer(this.components);
            this.S32 = new System.Windows.Forms.Timer(this.components);
            this.S33 = new System.Windows.Forms.Timer(this.components);
            this.S41 = new System.Windows.Forms.Timer(this.components);
            this.S42 = new System.Windows.Forms.Timer(this.components);
            this.S43 = new System.Windows.Forms.Timer(this.components);
            this.S51 = new System.Windows.Forms.Timer(this.components);
            this.S52 = new System.Windows.Forms.Timer(this.components);
            this.S53 = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.Box1 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.axWindowsMediaPlayer1 = new AxWMPLib.AxWindowsMediaPlayer();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Box1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axWindowsMediaPlayer1)).BeginInit();
            this.SuspendLayout();
            // 
            // Startbtn
            // 
            this.Startbtn.BackColor = System.Drawing.Color.Red;
            this.Startbtn.Font = new System.Drawing.Font("Wide Latin", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Startbtn.ForeColor = System.Drawing.Color.Gold;
            this.Startbtn.Location = new System.Drawing.Point(581, 422);
            this.Startbtn.Name = "Startbtn";
            this.Startbtn.Size = new System.Drawing.Size(74, 43);
            this.Startbtn.TabIndex = 0;
            this.Startbtn.Text = "SPIN 250К";
            this.Startbtn.UseVisualStyleBackColor = false;
            this.Startbtn.Click += new System.EventHandler(this.Startbtn_Click);
            // 
            // O1
            // 
            this.O1.AutoSize = true;
            this.O1.BackColor = System.Drawing.Color.White;
            this.O1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.O1.Font = new System.Drawing.Font("Old English Text MT", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.O1.Location = new System.Drawing.Point(238, 161);
            this.O1.Name = "O1";
            this.O1.Size = new System.Drawing.Size(69, 79);
            this.O1.TabIndex = 1;
            this.O1.Text = "1";
            // 
            // O2
            // 
            this.O2.AutoSize = true;
            this.O2.BackColor = System.Drawing.Color.White;
            this.O2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.O2.Font = new System.Drawing.Font("Old English Text MT", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.O2.Location = new System.Drawing.Point(238, 240);
            this.O2.Name = "O2";
            this.O2.Size = new System.Drawing.Size(69, 79);
            this.O2.TabIndex = 2;
            this.O2.Text = "0";
            // 
            // O3
            // 
            this.O3.AutoSize = true;
            this.O3.BackColor = System.Drawing.Color.White;
            this.O3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.O3.Font = new System.Drawing.Font("Old English Text MT", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.O3.Location = new System.Drawing.Point(238, 319);
            this.O3.Name = "O3";
            this.O3.Size = new System.Drawing.Size(69, 79);
            this.O3.TabIndex = 3;
            this.O3.Text = "3";
            // 
            // D1
            // 
            this.D1.Interval = 10;
            this.D1.Tick += new System.EventHandler(this.Dvig1_Tick);
            // 
            // D2
            // 
            this.D2.Interval = 10;
            this.D2.Tick += new System.EventHandler(this.Dvig2_Tick);
            // 
            // D3
            // 
            this.D3.Interval = 10;
            this.D3.Tick += new System.EventHandler(this.Dvig3_Tick);
            // 
            // S1
            // 
            this.S1.Interval = 7200;
            this.S1.Tick += new System.EventHandler(this.Stop1_Tick);
            // 
            // S2
            // 
            this.S2.Interval = 4000;
            this.S2.Tick += new System.EventHandler(this.Stop2_Tick);
            // 
            // S3
            // 
            this.S3.Interval = 7215;
            this.S3.Tick += new System.EventHandler(this.Stop3_Tick);
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.BackColor = System.Drawing.Color.Black;
            this.Label2.Font = new System.Drawing.Font("Old English Text MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.ForeColor = System.Drawing.Color.Gold;
            this.Label2.Location = new System.Drawing.Point(475, 431);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(45, 20);
            this.Label2.TabIndex = 5;
            this.Label2.Text = "1000";
            this.Label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // O21
            // 
            this.O21.AutoSize = true;
            this.O21.BackColor = System.Drawing.Color.White;
            this.O21.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.O21.Font = new System.Drawing.Font("Old English Text MT", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.O21.Location = new System.Drawing.Point(322, 161);
            this.O21.Name = "O21";
            this.O21.Size = new System.Drawing.Size(69, 79);
            this.O21.TabIndex = 8;
            this.O21.Text = "5";
            // 
            // O22
            // 
            this.O22.AutoSize = true;
            this.O22.BackColor = System.Drawing.Color.White;
            this.O22.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.O22.Font = new System.Drawing.Font("Old English Text MT", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.O22.Location = new System.Drawing.Point(322, 240);
            this.O22.Name = "O22";
            this.O22.Size = new System.Drawing.Size(69, 79);
            this.O22.TabIndex = 9;
            this.O22.Text = "7";
            // 
            // O53
            // 
            this.O53.AutoSize = true;
            this.O53.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.O53.Font = new System.Drawing.Font("Old English Text MT", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.O53.Location = new System.Drawing.Point(581, 319);
            this.O53.Name = "O53";
            this.O53.Size = new System.Drawing.Size(69, 79);
            this.O53.TabIndex = 11;
            this.O53.Text = "1";
            // 
            // O52
            // 
            this.O52.AutoSize = true;
            this.O52.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.O52.Font = new System.Drawing.Font("Old English Text MT", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.O52.Location = new System.Drawing.Point(581, 240);
            this.O52.Name = "O52";
            this.O52.Size = new System.Drawing.Size(69, 79);
            this.O52.TabIndex = 12;
            this.O52.Text = "0";
            // 
            // O51
            // 
            this.O51.AutoSize = true;
            this.O51.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.O51.Font = new System.Drawing.Font("Old English Text MT", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.O51.Location = new System.Drawing.Point(581, 161);
            this.O51.Name = "O51";
            this.O51.Size = new System.Drawing.Size(69, 79);
            this.O51.TabIndex = 13;
            this.O51.Text = "3";
            // 
            // O32
            // 
            this.O32.AutoSize = true;
            this.O32.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.O32.Font = new System.Drawing.Font("Old English Text MT", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.O32.Location = new System.Drawing.Point(409, 240);
            this.O32.Name = "O32";
            this.O32.Size = new System.Drawing.Size(69, 79);
            this.O32.TabIndex = 14;
            this.O32.Text = "7";
            // 
            // O43
            // 
            this.O43.AutoSize = true;
            this.O43.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.O43.Font = new System.Drawing.Font("Old English Text MT", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.O43.Location = new System.Drawing.Point(496, 319);
            this.O43.Name = "O43";
            this.O43.Size = new System.Drawing.Size(69, 79);
            this.O43.TabIndex = 15;
            this.O43.Text = "5";
            // 
            // O42
            // 
            this.O42.AutoSize = true;
            this.O42.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.O42.Font = new System.Drawing.Font("Old English Text MT", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.O42.Location = new System.Drawing.Point(496, 240);
            this.O42.Name = "O42";
            this.O42.Size = new System.Drawing.Size(69, 79);
            this.O42.TabIndex = 16;
            this.O42.Text = "7";
            // 
            // O41
            // 
            this.O41.AutoSize = true;
            this.O41.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.O41.Font = new System.Drawing.Font("Old English Text MT", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.O41.Location = new System.Drawing.Point(495, 161);
            this.O41.Name = "O41";
            this.O41.Size = new System.Drawing.Size(69, 79);
            this.O41.TabIndex = 17;
            this.O41.Text = "9";
            // 
            // O31
            // 
            this.O31.AutoSize = true;
            this.O31.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.O31.Font = new System.Drawing.Font("Old English Text MT", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.O31.Location = new System.Drawing.Point(409, 161);
            this.O31.Name = "O31";
            this.O31.Size = new System.Drawing.Size(69, 79);
            this.O31.TabIndex = 18;
            this.O31.Text = "0";
            // 
            // O33
            // 
            this.O33.AutoSize = true;
            this.O33.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.O33.Font = new System.Drawing.Font("Old English Text MT", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.O33.Location = new System.Drawing.Point(409, 319);
            this.O33.Name = "O33";
            this.O33.Size = new System.Drawing.Size(69, 79);
            this.O33.TabIndex = 19;
            this.O33.Text = "0";
            // 
            // O23
            // 
            this.O23.AutoSize = true;
            this.O23.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.O23.Font = new System.Drawing.Font("Old English Text MT", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.O23.Location = new System.Drawing.Point(322, 319);
            this.O23.Name = "O23";
            this.O23.Size = new System.Drawing.Size(69, 79);
            this.O23.TabIndex = 20;
            this.O23.Text = "9";
            // 
            // D21
            // 
            this.D21.Interval = 10;
            this.D21.Tick += new System.EventHandler(this.D21_Tick);
            // 
            // D22
            // 
            this.D22.Interval = 10;
            this.D22.Tick += new System.EventHandler(this.D22_Tick);
            // 
            // D23
            // 
            this.D23.Interval = 10;
            this.D23.Tick += new System.EventHandler(this.D23_Tick);
            // 
            // D31
            // 
            this.D31.Interval = 10;
            this.D31.Tick += new System.EventHandler(this.D31_Tick);
            // 
            // D32
            // 
            this.D32.Interval = 10;
            this.D32.Tick += new System.EventHandler(this.D32_Tick);
            // 
            // D33
            // 
            this.D33.Interval = 10;
            this.D33.Tick += new System.EventHandler(this.D33_Tick);
            // 
            // D41
            // 
            this.D41.Interval = 10;
            this.D41.Tick += new System.EventHandler(this.D41_Tick);
            // 
            // D42
            // 
            this.D42.Interval = 10;
            this.D42.Tick += new System.EventHandler(this.D42_Tick);
            // 
            // D43
            // 
            this.D43.Interval = 10;
            this.D43.Tick += new System.EventHandler(this.D43_Tick);
            // 
            // D51
            // 
            this.D51.Interval = 10;
            this.D51.Tick += new System.EventHandler(this.D51_Tick);
            // 
            // D52
            // 
            this.D52.Interval = 10;
            this.D52.Tick += new System.EventHandler(this.D52_Tick);
            // 
            // D53
            // 
            this.D53.Interval = 10;
            this.D53.Tick += new System.EventHandler(this.D53_Tick);
            // 
            // S21
            // 
            this.S21.Interval = 3010;
            this.S21.Tick += new System.EventHandler(this.S21_Tick);
            // 
            // S22
            // 
            this.S22.Interval = 6500;
            this.S22.Tick += new System.EventHandler(this.S22_Tick);
            // 
            // S23
            // 
            this.S23.Interval = 3000;
            this.S23.Tick += new System.EventHandler(this.S23_Tick);
            // 
            // S31
            // 
            this.S31.Interval = 6500;
            this.S31.Tick += new System.EventHandler(this.S31_Tick);
            // 
            // S32
            // 
            this.S32.Interval = 7800;
            this.S32.Tick += new System.EventHandler(this.S32_Tick);
            // 
            // S33
            // 
            this.S33.Interval = 6500;
            this.S33.Tick += new System.EventHandler(this.S33_Tick);
            // 
            // S41
            // 
            this.S41.Interval = 3015;
            this.S41.Tick += new System.EventHandler(this.S41_Tick);
            // 
            // S42
            // 
            this.S42.Interval = 6500;
            this.S42.Tick += new System.EventHandler(this.S42_Tick);
            // 
            // S43
            // 
            this.S43.Interval = 3000;
            this.S43.Tick += new System.EventHandler(this.S43_Tick);
            // 
            // S51
            // 
            this.S51.Interval = 7200;
            this.S51.Tick += new System.EventHandler(this.S51_Tick);
            // 
            // S52
            // 
            this.S52.Interval = 4000;
            this.S52.Tick += new System.EventHandler(this.S52_Tick);
            // 
            // S53
            // 
            this.S53.Interval = 7300;
            this.S53.Tick += new System.EventHandler(this.S53_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Black;
            this.label1.Font = new System.Drawing.Font("Old English Text MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Gold;
            this.label1.Location = new System.Drawing.Point(387, 431);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 20);
            this.label1.TabIndex = 22;
            this.label1.Text = "0";
            // 
            // Box1
            // 
            this.Box1.BackColor = System.Drawing.Color.Black;
            this.Box1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Box1.Location = new System.Drawing.Point(275, 422);
            this.Box1.Name = "Box1";
            this.Box1.Size = new System.Drawing.Size(70, 43);
            this.Box1.TabIndex = 24;
            this.Box1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Я_сошел_с_ума_1.Properties.Resources.screen_04;
            this.pictureBox1.Location = new System.Drawing.Point(-1, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(894, 510);
            this.pictureBox1.TabIndex = 23;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // axWindowsMediaPlayer1
            // 
            this.axWindowsMediaPlayer1.Enabled = true;
            this.axWindowsMediaPlayer1.Location = new System.Drawing.Point(275, 422);
            this.axWindowsMediaPlayer1.Name = "axWindowsMediaPlayer1";
            this.axWindowsMediaPlayer1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axWindowsMediaPlayer1.OcxState")));
            this.axWindowsMediaPlayer1.Size = new System.Drawing.Size(42, 32);
            this.axWindowsMediaPlayer1.TabIndex = 25;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Red;
            this.button1.Font = new System.Drawing.Font("Wide Latin", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Gold;
            this.button1.Location = new System.Drawing.Point(275, 422);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(70, 43);
            this.button1.TabIndex = 26;
            this.button1.Text = "MUZ";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(890, 504);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Box1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.O23);
            this.Controls.Add(this.O33);
            this.Controls.Add(this.O31);
            this.Controls.Add(this.O41);
            this.Controls.Add(this.O42);
            this.Controls.Add(this.O43);
            this.Controls.Add(this.O32);
            this.Controls.Add(this.O51);
            this.Controls.Add(this.O52);
            this.Controls.Add(this.O53);
            this.Controls.Add(this.O22);
            this.Controls.Add(this.O21);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.O3);
            this.Controls.Add(this.O2);
            this.Controls.Add(this.O1);
            this.Controls.Add(this.Startbtn);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.axWindowsMediaPlayer1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Вулкан*****";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Box1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axWindowsMediaPlayer1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Startbtn;
        private System.Windows.Forms.Label O1;
        private System.Windows.Forms.Label O2;
        private System.Windows.Forms.Label O3;
        private System.Windows.Forms.Timer D1;
        private System.Windows.Forms.Timer D2;
        private System.Windows.Forms.Timer D3;
        private System.Windows.Forms.Timer S1;
        private System.Windows.Forms.Timer S2;
        private System.Windows.Forms.Timer S3;
        private System.Windows.Forms.Label Label2;
        private System.Windows.Forms.Label O21;
        private System.Windows.Forms.Label O22;
        private System.Windows.Forms.Label O53;
        private System.Windows.Forms.Label O52;
        private System.Windows.Forms.Label O51;
        private System.Windows.Forms.Label O32;
        private System.Windows.Forms.Label O43;
        private System.Windows.Forms.Label O42;
        private System.Windows.Forms.Label O41;
        private System.Windows.Forms.Label O31;
        private System.Windows.Forms.Label O33;
        private System.Windows.Forms.Label O23;
        private System.Windows.Forms.Timer D21;
        private System.Windows.Forms.Timer D22;
        private System.Windows.Forms.Timer D23;
        private System.Windows.Forms.Timer D31;
        private System.Windows.Forms.Timer D32;
        private System.Windows.Forms.Timer D33;
        private System.Windows.Forms.Timer D41;
        private System.Windows.Forms.Timer D42;
        private System.Windows.Forms.Timer D43;
        private System.Windows.Forms.Timer D51;
        private System.Windows.Forms.Timer D52;
        private System.Windows.Forms.Timer D53;
        private System.Windows.Forms.Timer S21;
        private System.Windows.Forms.Timer S22;
        private System.Windows.Forms.Timer S23;
        private System.Windows.Forms.Timer S31;
        private System.Windows.Forms.Timer S32;
        private System.Windows.Forms.Timer S33;
        private System.Windows.Forms.Timer S41;
        private System.Windows.Forms.Timer S42;
        private System.Windows.Forms.Timer S43;
        private System.Windows.Forms.Timer S51;
        private System.Windows.Forms.Timer S52;
        private System.Windows.Forms.Timer S53;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox Box1;
        private AxWMPLib.AxWindowsMediaPlayer axWindowsMediaPlayer1;
        private System.Windows.Forms.Button button1;
    }
}

